package logger

import (
	"bufio"
	"log"
	"os"
	"strings"
)

type Logger struct {
	stdErr *bufio.Writer
	stdOut *bufio.Writer

	file *os.File

	data   chan string
	cancel chan os.Signal
}

func NewLogger() *Logger {
	return &Logger{
		stdErr: bufio.NewWriter(os.Stderr),
		stdOut: bufio.NewWriter(os.Stdout),
		data:   make(chan string, 10),
		cancel: make(chan os.Signal, 1),
	}
}

func (l *Logger) Start() {
	var err error
	l.file, err = os.OpenFile("history.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	l.WriteInfo("Logger configurated", "LOGGER")
	// signal.Notify(l.cancel, os.Interrupt)
	l.listenAndServe()
}

func (l *Logger) listenAndServe() {
	l.WriteOK("Logger started", "LOGGER")
	for {
		select {
		case <-l.cancel:
			l.gracefulShutdown()
		case msg, ok := <-l.data:
			if !ok {
				continue
			}
			if strings.Contains(msg, "ERR") {
				l.stdErr.WriteString(msg)
				l.stdErr.Flush()
			} else {
				l.stdOut.WriteString(msg)
				l.stdOut.Flush()
			}

			_, err := l.file.WriteString(msg)
			if err != nil {
				l.WriteErr(err, "LOGGER")
			}
		}
	}
}

func (l *Logger) Done() {
	l.cancel <- os.Interrupt
}

func (l *Logger) gracefulShutdown() {
	l.stdErr.Flush()
	l.stdOut.Flush()
	l.file.Close()
}
