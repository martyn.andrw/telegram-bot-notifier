package logger

import (
	"fmt"
	"time"
)

const (
	// reset = "\033[0m"
	// red   = "\033[31m"
	// green = "\033[32m"
	// cyan  = "\033[36m"

	errStr  = "\033[31mERR\033[0m"
	infoStr = "\033[36mINFO\033[0m"
	okStr   = "\033[32mOK\033[0m"
)

func (l *Logger) WriteErr(err error, who string) {
	msg := fmt.Sprintf("%s\t%s\t%s\t%s\n", time.Now().Format(time.UnixDate), errStr, who, err)
	l.data <- msg
}

func (l *Logger) WriteInfo(info string, who string) {
	msg := fmt.Sprintf("%s\t%s\t%s\t%s\n", time.Now().Format(time.UnixDate), infoStr, who, info)
	l.data <- msg
}

func (l *Logger) WriteOK(info string, who string) {
	msg := fmt.Sprintf("%s\t%s\t%s\t%s\n", time.Now().Format(time.UnixDate), okStr, who, info)
	l.data <- msg
}
