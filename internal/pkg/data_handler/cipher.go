package data_handler

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"strconv"
)

func encode(str string, chat_id int) (string, error) {
	text := []byte(str)
	key := []byte(strconv.Itoa(chat_id))
	inClearData := []byte("Some Clear Data")

	key = sha256.New().Sum(key)
	fmt.Println(string(key[:32]))

	c, err := aes.NewCipher(key[:32])
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}

	return string(gcm.Seal(nonce, nonce, text, inClearData)), nil
}

func decode(str string, chat_id int) (string, error) {
	key := []byte(strconv.Itoa(chat_id))
	ciphertext := []byte(str)
	inClearData := []byte("Some Clear Data")

	key = sha256.New().Sum(key)
	fmt.Println(string(key[:32]))

	c, err := aes.NewCipher(key[:32])
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return "", errors.New("len(ciphertext) < nonceSize")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	clearStuff := make([]byte, 50)
	plaintext, err := gcm.Open(clearStuff, nonce, ciphertext, inClearData)
	if err != nil {
		return "", err
	}
	return string(plaintext), nil
}
