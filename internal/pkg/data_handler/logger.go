package data_handler

type Logger interface {
	WriteErr(error, string)
	WriteInfo(string, string)
	WriteOK(string, string)
	Done()
}
