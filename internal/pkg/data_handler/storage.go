package data_handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	fileBackup = "db.json"
)

type RandStorage struct {
	Notifications map[string]*Notification
	Users         map[int][]string
	mutex         sync.RWMutex
	logger        Logger
}

func NewRandStorage(lggr Logger) *RandStorage {
	r := &RandStorage{}
	body, err := os.ReadFile(fileBackup)
	if err != nil {
		lggr.WriteErr(err, "STORAGE")
	}
	r.Unmarshal(body)

	if r.Notifications == nil {
		r.Notifications = make(map[string]*Notification)
	}
	if r.Users == nil {
		r.Users = make(map[int][]string)
	}
	r.logger = lggr

	return r
}

func (r *RandStorage) AddRandomNotification(title, text, first, last string, chat_id, times int, expires time.Time) error {
	str := fmt.Sprintf("%d-%s", chat_id, title)

	r.mutex.Lock()
	defer r.mutex.Unlock()

	_, exist := r.Notifications[str]
	if exist {
		return errors.New("notification already exists")
	}

	clockF, err := ParseClock(first)
	if err != nil {
		return err
	}
	clockL, err := ParseClock(last)
	if err != nil {
		return err
	}

	c_text, err := encode(text, chat_id)
	if err != nil {
		return err
	}

	r.Notifications[str] = &Notification{
		Title:    title,
		Times:    times,
		Chat_id:  chat_id,
		Text:     c_text,
		dayCount: 0,
		First:    clockF,
		Last:     clockL,
		Expires:  expires,
	}
	r.Users[chat_id] = append(r.Users[chat_id], str)
	return nil
}

func (r *RandStorage) GetRandomNotifications() []map[string]interface{} {
	res := []map[string]interface{}{}

	r.mutex.RLock()
	defer r.mutex.RUnlock()

	for key, value := range r.Notifications {
		if value.IsExpired() {
			delete(r.Notifications, key)
			continue
		}

		title, text, chat, ok := value.SendingInfo()
		if ok {
			d_text, _ := decode(text, chat)

			m := make(map[string]interface{})
			m["title"] = title
			m["text"] = d_text
			m["chat_id"] = chat
			res = append(res, m)
		}
	}
	return res
}

type Notification struct {
	Times   int
	Chat_id int
	Text    string
	Title   string

	dayCount int

	First   Clock
	Last    Clock
	Expires time.Time

	mutex sync.RWMutex
}

type Clock struct {
	H int
	M int
	S int
}

func (c Clock) Before(clock Clock) bool {
	if c.H > clock.H {
		return false
	}
	if c.H < clock.H {
		return true
	}

	if c.M > clock.M {
		return false
	}
	if c.M < clock.M {
		return true
	}

	return c.S < clock.S
}

func (c Clock) After(clock Clock) bool {
	if c.H < clock.H {
		return false
	}
	if c.H > clock.H {
		return true
	}

	if c.M < clock.M {
		return false
	}
	if c.M > clock.M {
		return true
	}

	return c.S > clock.S
}

func (c Clock) String() string {
	h_str := strconv.Itoa(c.H)
	m_str := strconv.Itoa(c.M)
	s_str := strconv.Itoa(c.S)

	if len(h_str) < 2 {
		h_str = "0" + h_str
	}
	if len(m_str) < 2 {
		m_str = "0" + m_str
	}
	if len(s_str) < 2 {
		s_str = "0" + s_str
	}

	res := fmt.Sprintf("%s:%s:%s", h_str, m_str, s_str)
	return res
}

func ParseClock(str string) (Clock, error) {
	if len(str) < 3 {
		return Clock{}, errors.New("<" + str + "> wrong format. use hh:mm:ss")
	}
	ind := strings.IndexRune(str, ':')
	if ind < 0 {
		return Clock{}, errors.New("<" + str + "> wrong format. use hh:mm:ss")
	}
	h_str := str[:ind]

	hour, err := strconv.Atoi(h_str)
	if err != nil {
		return Clock{}, err
	}
	if hour > 23 || hour < 0 {
		return Clock{}, errors.New("<" + str + "> wrong format. use hh:mm:ss")
	}

	ind1 := strings.IndexRune(str[ind+1:], ':')
	if ind1 == -1 {
		ind1 = len(str[ind+1:])
	}
	m_str := str[ind+1:][:ind1]

	minute, err := strconv.Atoi(m_str)
	if err != nil {
		return Clock{}, err
	}
	if minute > 59 || minute < 0 {
		return Clock{}, errors.New("<" + str + "> wrong format. use hh:mm:ss")
	}

	if len(str[ind1+1:]) == 0 {
		return Clock{H: hour, M: minute}, nil
	}

	ind2 := strings.IndexRune(str[ind1+1:], ':')
	s_str := str[ind1+1:][ind2+1:]

	seconds, err := strconv.Atoi(s_str)
	if err != nil {
		return Clock{}, err
	}
	if seconds > 59 || seconds < 0 {
		return Clock{}, errors.New("<" + str + "> wrong format. use hh:mm:ss")
	}
	return Clock{H: hour, M: minute, S: seconds}, nil
}

func (n *Notification) SendingInfo() (string, string, int, bool) {
	n.mutex.Lock()
	defer n.mutex.Unlock()

	nh, nm, ns := time.Now().Clock()
	now := Clock{H: nh, M: nm, S: ns}

	if (n.First.After(n.Last) && now.Before(n.First) && now.After(n.Last) ||
		n.First.Before(n.Last) && now.After(n.First) && now.Before(n.Last)) &&
		n.dayCount < n.Times {

		allSecs := 1

		if n.First.Before(n.Last) || now.Before(n.Last) {
			allSecs = (n.Last.H-nh)*60*60 + (n.Last.M-nm)*60 + (n.Last.S-ns)*1
		} else {
			allSecs = (23-nh)*60*60 + (59-nm)*60 + (59-ns)*1 + (n.First.H-nh)*60*60 + (n.First.M-nm)*60 + (n.First.S-ns)*1
		}

		// todo
		probability := float64(n.Times-n.dayCount) * 60 / float64(allSecs)
		dice := rand.Float64()

		if dice <= probability {
			n.dayCount++
			return n.Title, n.Text, n.Chat_id, true
		}
	}

	return "", "", 0, false
}

func (n *Notification) IsExpired() bool {
	n.mutex.RLock()
	defer n.mutex.RUnlock()
	return time.Now().After(n.Expires)
}

func (r *RandStorage) ShowAll(chat_id int) ([]map[string]interface{}, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	keys, exists := r.Users[chat_id]
	if !exists {
		return nil, errors.New("how is it possible")
	}

	res := []map[string]interface{}{}
	for _, key := range keys {
		notification, exists := r.Notifications[key]
		if !exists {
			r.logger.WriteErr(errors.New("todo section"), "STORAGE-TODO")
			continue
		}

		d_text, err := decode(notification.Text, chat_id)
		if err != nil {
			return nil, err
		}

		buf := make(map[string]interface{})
		buf["id"] = key
		buf["title"] = notification.Title
		buf["text"] = d_text
		buf["start"] = notification.First.String()
		buf["end"] = notification.Last.String()
		buf["expires"] = notification.Expires
		buf["count"] = notification.Times

		res = append(res, buf)
	}

	return res, nil
}

func (r *RandStorage) Clear(chat_id int) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	if _, exists := r.Users[chat_id]; !exists {
		return errors.New("how is it possible")
	}

	for _, key := range r.Users[chat_id] {
		delete(r.Notifications, key)
	}
	r.Users[chat_id] = []string{}
	return nil
}

func (r *RandStorage) Delete(chat_id int, id string) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	if _, exists := r.Users[chat_id]; !exists {
		return errors.New("how is it possible")
	}

	for i := range r.Users[chat_id] {
		if r.Users[chat_id][i] == id {
			r.Users[chat_id] = append(r.Users[chat_id][:i], r.Users[chat_id][i+1:]...)
			delete(r.Notifications, id)
			return nil
		}
	}
	return errors.New("wrong id")
}

func (r *RandStorage) Save() error {
	err := os.Remove(fileBackup)
	if err != nil {
		r.logger.WriteErr(err, "STORAGE")
	}
	file, err := os.OpenFile(fileBackup, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	r.mutex.RLock()
	defer r.mutex.RUnlock()

	body, err := r.Marshal()
	if err != nil {
		return err
	}
	file.Write(body)
	return nil
}

func (r *RandStorage) Marshal() ([]byte, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	return json.Marshal(r)
}
func (r *RandStorage) Unmarshal(body []byte) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return json.Unmarshal(body, &r)
}

func (r *RandStorage) Done() {
	defer r.logger.WriteOK("Storage closed.", "STORAGE")
	err := r.Save()
	if err != nil {
		r.logger.WriteErr(err, "STORAGE")
	}
}
