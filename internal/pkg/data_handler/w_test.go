package data_handler

import "testing"

func TestParse1(t *testing.T) {
	str := "22:00:00"
	expected := Clock{H: 22, M: 0, S: 0}
	result, err := ParseClock(str)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	if result.H != expected.H || result.M != expected.M || result.S != expected.S {
		t.Errorf("Expected value: %+v, but got: %+v", expected, result)
	}
}

func TestParse2(t *testing.T) {
	str := "2:00"
	expected := Clock{H: 2, M: 0, S: 0}
	result, err := ParseClock(str)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	if result.H != expected.H || result.M != expected.M || result.S != expected.S {
		t.Errorf("Expected value: %+v, but got: %+v", expected, result)
	}
}
