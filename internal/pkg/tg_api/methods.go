package tg_api

import "encoding/json"

func (m *Message) Command() string {
	for _, entity := range m.Entities {
		if entity.Type == "bot_command" {
			return m.Text
		}
	}
	return ""
}

func (m *Message) Marshal() ([]byte, error) {
	bytes, err := json.Marshal(m)
	return bytes, err
}

func (m *Message) MsgText() string {
	return m.Text
}

func (m *Message) Unmarshal(b []byte) error {
	return json.Unmarshal(b, m)
}

func (m *Messages) Unmarshal(b []byte) error {
	return json.Unmarshal(b, m)
}

func (m *Messages) Len() int {
	return len(m.Array)
}
