package tg_api

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"
)

type Bot struct {
	token   string
	getUrl  string
	sendUrl string
	offset  int
	delay   time.Duration

	storage Storage

	lggr Logger

	cancel     chan os.Signal
	tickerReq  *time.Ticker
	tickerSend *time.Ticker
	ticker24   *time.Ticker
}

func NewBot(stor Storage, cfg Config, logger Logger) *Bot { // todo logger
	d, err := cfg.Delay()
	if logger != nil && err != nil {
		logger.WriteErr(err, "BOT(NewBot)")
	} else if err != nil {
		log.Println(err)
	}

	return &Bot{
		token:      cfg.Token(),
		getUrl:     fmt.Sprintf("https://api.telegram.org/bot%s/getUpdates", cfg.Token()),
		sendUrl:    fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage", cfg.Token()),
		delay:      d,
		storage:    stor,
		lggr:       logger,
		cancel:     make(chan os.Signal, 1),
		tickerReq:  time.NewTicker(d),
		tickerSend: time.NewTicker(d),
		ticker24:   time.NewTicker(24 * time.Hour),
	}
}

func (b *Bot) Start() {
	b.logInfo("Bot configurated")
	signal.Notify(b.cancel, os.Interrupt)
	b.listenAndServe()
}

func (b *Bot) listenAndServe() {
	b.logOK("Bot started")
	for {
		select {
		case <-b.cancel:
			b.logInfo("Bot got interrupt signal")
			b.gracefulShutdown()
			return
		case <-b.tickerReq.C:
			msgs := b.getUpdates()
			wg := sync.WaitGroup{}
			wg.Add(msgs.Len())

			if len(msgs.Array) > 0 {
				b.logInfo("Got updates")
			}

			for _, update := range msgs.Array {
				go b.messageHandler(update.Msg, &wg)
				if update.Update_id >= b.offset {
					b.offset = update.Update_id + 1
				}
			}
			wg.Wait()
		case <-b.tickerSend.C:
			for _, smt := range b.storage.GetRandomNotifications() {
				msg := NewMessage()
				msg.Chat_ID = smt["chat_id"].(int)
				msg.Text = smt["text"].(string)
				b.SendMessage(msg)
			}
		case <-b.ticker24.C:
			b.storage.Save()
		}
	}
}

func (b *Bot) Done() {
	b.cancel <- os.Interrupt
}

func (b *Bot) getUpdates() *Messages {
	url := fmt.Sprintf("%s?offset=%d", b.getUrl, b.offset)
	resp, err := http.Get(url)
	b.checkError(err)

	body, err := io.ReadAll(resp.Body)
	b.checkError(err)
	defer resp.Body.Close()

	msgs := NewMessages()
	err = msgs.Unmarshal(body)
	b.checkError(err)
	return msgs
}

func (b *Bot) messageHandler(msg Message, wg *sync.WaitGroup) {
	defer wg.Done()
	if strings.Contains(msg.MsgText(), `/help`) {
		b.Help(msg.From.ID)
	}

	switch {
	case strings.Contains(msg.Text, "/admin_edit_help"):
		b.adminEditHelp(&msg)
	case strings.Contains(msg.Text, "/new_notification"):
		b.AddRandomNotification(&msg)
	case strings.Contains(msg.Text, "/delete_notification"):
		b.DeleteNotification(&msg)
	case strings.Contains(msg.Text, "/clear_notifications"):
		b.ClearNotifications(&msg)
	case strings.Contains(msg.Text, "/show_notifications"):
		b.ShowNotifications(&msg)
	}
}

func (b *Bot) SendMessage(msg *Message) {
	if msg == nil {
		return
	}

	body, err := msg.Marshal()
	b.checkError(err)

	http.Post(b.sendUrl, "application/json", bytes.NewBuffer(body))
}

func (b *Bot) gracefulShutdown() {
	if b.storage != nil {
		b.storage.Done()
	}
	if b.lggr != nil {
		b.lggr.Done()
	}
}
