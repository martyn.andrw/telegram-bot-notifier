package tg_api

type Message struct {
	Message_id int             `json:"message_id"` // Unique message identifier inside this chat
	From       User            `json:"from"`       // Optional. Sender of the message; empty for messages sent to channels. For backward compatibility, the field contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
	Chat       Chat            `json:"chat"`
	Chat_ID    int             `json:"chat_id"`  // Conversation the message belongs to
	Text       string          `json:"text"`     // Optional. For sending messages.
	Entities   []MessageEntity `json:"entities"` // Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
	// animation 	Animation // Optional. Message is an animation, information about the animation. For backward compatibility, when this field is set, the document field will also be set
	// audio 	Audio // Optional. Message is an audio file, information about the file
	// document 	Document // Optional. Message is a general file, information about the file
	// photo 	Array of PhotoSize // Optional. Message is a photo, available sizes of the photo
	// sticker 	Sticker // Optional. Message is a sticker, information about the sticker
	// video 	Video // Optional. Message is a video, information about the video
	// video_note 	VideoNote // Optional. Message is a video note, information about the video message
	// voice 	Voice // Optional. Message is a voice message, information about the file
	// Caption string // Optional. Caption for the animation, audio, document, photo, video or voice
	// caption_entities Array of MessageEntity // Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
	// contact 	Contact // Optional. Message is a shared contact, information about the contact
	// dice 	Dice // Optional. Message is a dice with random value
	// game 	Game // Optional. Message is a game, information about the game. More about games »
	// poll 	Poll // Optional. Message is a native poll, information about the poll
	// venue 	Venue // Optional. Message is a venue, information about the venue. For backward compatibility, when this field is set, the location field will also be set
}

type User struct {
	ID            int    `json:"id"`            // Unique identifier for this user or bot. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit integer or double-precision float type are safe for storing this identifier.
	Is_bot        bool   `json:"is_bot"`        // True, if this user is a bot
	First_name    string `json:"first_name"`    // User's or bot's first name
	Last_name     string `json:"last_name"`     // Optional. User's or bot's last name
	Username      string `json:"username"`      // Optional. User's or bot's username
	Language_code string `json:"language_code"` // Optional. IETF language tag of the user's language
	Is_premium    bool   `json:"is_premium"`    // Optional. True, if this user is a Telegram Premium user
	// Added_to_attachment_menu    bool   `json:"added_to_attachment_menu"`    // Optional. True, if this user added the bot to the attachment menu
	// Can_join_groups             bool   `json:"can_join_groups"`             // Optional. True, if the bot can be invited to groups. Returned only in getMe.
	// Can_read_all_group_messages bool   `json:"can_read_all_group_messages"` // Optional. True, if privacy mode is disabled for the bot. Returned only in getMe.
	// Supports_inline_queries     bool   `json:"supports_inline_queries"`     // Optional. True, if the bot supports inline queries. Returned only in getMe.
}

type Chat struct {
	ID         int    `json:"id"`         // Unique identifier for this chat. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
	Type       string `json:"type"`       // Type of chat, can be either “private”, “group”, “supergroup” or “channel”
	Title      string `json:"title"`      // Optional. Title, for supergroups, channels and group chats
	Username   string `json:"username"`   // Optional. Username, for private chats, supergroups and channels if available
	First_name string `json:"first_name"` // Optional. First name of the other party in a private chat
	Last_name  string `json:"last_name"`  // Optional. Last name of the other party in a private chat
}

type MessageEntity struct {
	Type            string `json:"type"`            // Type of the entity. Currently, can be “mention” (@username), “hashtag” (#hashtag), “cashtag” ($USD), “bot_command” (/start@jobs_bot), “url” (https://telegram.org), “email” (do-not-reply@telegram.org), “phone_number” (+1-212-555-0123), “bold” (bold text), “italic” (italic text), “underline” (underlined text), “strikethrough” (strikethrough text), “spoiler” (spoiler message), “code” (monowidth string), “pre” (monowidth block), “text_link” (for clickable text URLs), “text_mention” (for users without usernames), “custom_emoji” (for inline custom emoji stickers)
	Offset          int    `json:"offset"`          // Offset in UTF-16 code units to the start of the entity
	Length          int    `json:"length"`          // Length of the entity in UTF-16 code units
	Url             string `json:"url"`             // Optional. For “text_link” only, URL that will be opened after user taps on the text
	User            User   `json:"user"`            // Optional. For “text_mention” only, the mentioned user
	Language        string `json:"language"`        // Optional. For “pre” only, the programming language of the entity text
	Custom_emoji_id string `json:"custom_emoji_id"` // Optional. For “custom_emoji” only, unique identifier of the custom emoji. Use getCustomEmojiStickers to get full information about the sticker
}

type Update struct {
	Update_id int     `json:"update_id"`
	Msg       Message `json:"message"`
}

type Messages struct {
	OK    bool     `json:"ok"`
	Array []Update `json:"result"`
}

func NewMessage() *Message {
	return &Message{}
}

func NewMessages() *Messages {
	return &Messages{Array: []Update{}}
}
