package tg_api

import (
	"log"
	"time"
)

type Logger interface {
	WriteErr(error, string)
	WriteInfo(string, string)
	WriteOK(string, string)
	Done()
}

type Config interface {
	Token() string
	Delay() (time.Duration, error)
}

type Storage interface {
	AddRandomNotification(string, string, string, string, int, int, time.Time) error
	ShowAll(int) ([]map[string]interface{}, error)
	GetRandomNotifications() []map[string]interface{}
	Delete(int, string) error
	Clear(int) error
	Save() error
	Done()
}

type Commands interface {
	Next() Commands
	Command() Command
}

type Command interface {
	Exec() // todo
}

type MessageIntrfc interface {
	Marshal() ([]byte, error)
}

func (b *Bot) checkError(err error) {
	if err != nil && b.lggr != nil {
		b.logErr(err)
	} else if err != nil {
		log.Println(err)
	}
}

func (b *Bot) logInfo(msg string) {
	b.lggr.WriteInfo(msg, "BOT")
}
func (b *Bot) logErr(err error) {
	b.lggr.WriteErr(err, "BOT")
}
func (b *Bot) logOK(msg string) {
	b.lggr.WriteOK(msg, "BOT")
}
