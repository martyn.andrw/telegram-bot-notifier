package tg_api

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func (b *Bot) Help(chat_id int) {
	url := fmt.Sprintf("%s?chat_id=%d", b.sendUrl, chat_id)
	body, err := os.ReadFile("templateHelp.json")
	b.checkError(err)

	if body == nil {
		msg := Message{Text: "Подмога не прибудет...."}
		body, err = msg.Marshal()
		b.checkError(err)
	}

	_, err = http.Post(url, "application/json", bytes.NewBuffer(body))
	b.checkError(err)
	b.errSender(err, chat_id)
}

func (b *Bot) AddRandomNotification(msg *Message) {
	flags, err := parseRandomFlags(msg)
	b.checkError(err)
	if !b.errSender(err, msg.From.ID) {
		err = b.storage.AddRandomNotification(flags.title, flags.text, flags.first, flags.last, msg.From.ID, flags.times, flags.expires)
		b.checkError(err)
		if !b.errSender(err, msg.From.ID) {
			b.logInfo("Added notification")
			b.SendMessage(&Message{Chat_ID: msg.From.ID, Text: fmt.Sprintf("Добавил. id дл удаления: %d-%s", msg.From.ID, flags.title)})
		}
	}
}

func (b *Bot) DeleteNotification(msg *Message) {
	if msg.Text == "/delete_notification" {
		b.SendMessage(&Message{Chat_ID: msg.From.ID, Text: "Необходимо указать id.\nВот список ваших уведомлений:"})
		b.ShowNotifications(msg)
		return
	}

	flags, err := parseRandomFlags(msg)
	b.checkError(err)
	if !b.errSender(err, msg.From.ID) {
		err = b.storage.Delete(msg.From.ID, flags.id)
		b.checkError(err)
		if !b.errSender(err, msg.From.ID) {
			b.logInfo("Deleted notification")
			b.SendMessage(&Message{Chat_ID: msg.From.ID, Text: "Удалено"})
		}
	}
}

func (b *Bot) ClearNotifications(msg *Message) {
	err := b.storage.Clear(msg.From.ID)
	b.checkError(err)
	if !b.errSender(err, msg.From.ID) {
		b.logInfo("Deleted all notifications")
		b.SendMessage(&Message{Chat_ID: msg.From.ID, Text: "Deleted all notifications"})
	}
}

func (b *Bot) ShowNotifications(msg *Message) {
	res, err := b.storage.ShowAll(msg.From.ID)
	b.checkError(err)
	if !b.errSender(err, msg.From.ID) {
		text := "Все напоминания:\n"
		for _, m := range res {
			for key, val := range m {
				text += fmt.Sprintf("%s: %v\n", key, val)
			}
			text += "\n"
		}

		b.SendMessage(&Message{Text: text, Chat_ID: msg.From.ID})
	}
}

type randomFlags struct {
	title   string
	text    string
	first   string
	last    string
	times   int
	id      string
	expires time.Time
}

func parseRandomFlags(msg *Message) (*randomFlags, error) {
	res := &randomFlags{}
	text_start := strings.Index(msg.Text, " ")
	var err error

	if strings.Contains(msg.Text, "-start ") {
		begin := strings.Index(msg.Text, "-start ") + 7
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end-begin < len("00:00") {
			return nil, errors.New("-start requires a value. use format hh:mm:ss or hh:mm")
		}
		res.first = msg.Text[begin:end]

		if end > text_start {
			text_start = end
		}
	} else {
		res.first = "00:00:00"
	}

	if strings.Contains(msg.Text, "-end ") {
		begin := strings.Index(msg.Text, "-end ") + 5
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end-begin < len("00:00") {
			return nil, errors.New("-end requires a value. use format hh:mm:ss or hh:mm")
		}
		res.last = msg.Text[begin:end]

		if end > text_start {
			text_start = end
		}
	} else {
		res.last = "23:59:59"
	}

	if strings.Contains(msg.Text, "-count ") {
		begin := strings.Index(msg.Text, "-count ") + 7
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end-begin < 1 {
			return nil, errors.New("-count requires a value. value must be greater than 0")
		}
		times_str := msg.Text[begin:end]
		res.times, err = strconv.Atoi(times_str)
		if err != nil {
			return nil, err
		}
		if res.times < 0 {
			return nil, errors.New("-count requires a non-negative value")
		}
		if res.times == 0 {
			return nil, errors.New("dude... why is -count 0?")
		}

		if end > text_start {
			text_start = end
		}
	} else {
		res.times = 20
	}

	if strings.Contains(msg.Text, "-expires ") {
		begin := strings.Index(msg.Text, "-expires ") + 9
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end-begin < len("dd:mm:yy") {
			return nil, errors.New("-expires requires a value. use format dd:mm:yy")
		}
		res.expires = time.Now().Add(500 * time.Hour) // todo

		if end > text_start {
			text_start = end
		}
	} else {
		res.expires = time.Now().Add(5000 * time.Hour)
	}

	if strings.Contains(msg.Text, "-id ") {
		begin := strings.Index(msg.Text, "-id ") + 4
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end <= begin {
			end = len(msg.Text)
		}
		if end-begin <= 0 {
			fmt.Println(end, begin)
			return nil, errors.New("-id requires a value")
		}
		res.id = msg.Text[begin:end]
	} else {
		res.id = msg.Text[text_start+1:]
	}

	if strings.Contains(msg.Text, "-title ") {
		begin := strings.Index(msg.Text, "-title ") + 7
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end-begin <= 0 {
			return nil, errors.New("-title requires a value")
		}
		res.title = msg.Text[begin:end]

		if end > text_start {
			text_start = end
		}
	} else {
		begin := text_start + 1
		end := strings.Index(msg.Text[begin:], " ") + begin
		if end-begin <= 0 {
			return nil, errors.New("-title requires a value")
		}
		res.title = msg.Text[begin:end]

		if end > text_start {
			text_start = end
		}
	}

	res.text = msg.Text[text_start+1:]
	return res, nil
}

type msgHelp struct {
	Text     string          `json:"text"`
	Entities []MessageEntity `json:"entities"`
}

func (b *Bot) adminEditHelp(msg *Message) {
	if !b.checkAccess(msg.From.ID) {
		return
	}

	start := strings.Index(msg.Text, " ")
	if start == -1 {
		b.logErr(errors.New("chel"))
		b.errSender(errors.New("chel"), msg.From.ID)
		return
	}

	ents := []MessageEntity{}
	for _, ent := range msg.Entities {
		if ent.Type != "bot_command" {
			ent.Offset -= len("/admin_edit_help")
			ents = append(ents, ent)
		}
	}

	msgH := msgHelp{
		Text:     msg.Text[start:],
		Entities: ents,
	}

	os.Remove("templateHelp.json")
	file, err := os.OpenFile("templateHelp.json", os.O_CREATE|os.O_WRONLY, 0666)
	if b.errSender(err, msg.From.ID) {
		return
	}
	defer file.Close()

	body, err := json.Marshal(msgH)
	if b.errSender(err, msg.From.ID) {
		return
	}
	_, err = file.Write(body)
	if b.errSender(err, msg.From.ID) {
		return
	}
	b.SendMessage(&Message{Text: "OK", Chat_ID: msg.From.ID})
	b.Help(msg.From.ID)
}

func (b *Bot) errSender(err error, chat_id int) bool {
	if err != nil {
		b.SendMessage(&Message{
			Text:    fmt.Sprintf("Error: %s", err.Error()),
			Chat_ID: chat_id,
			Entities: []MessageEntity{{
				Offset: len("Error "),
				Length: len(err.Error()) + 1,
				Type:   "italic",
			},
			},
		})
		return true
	}
	return false
}

func (b *Bot) checkAccess(chat_id int) bool {
	if chat_id != 392378632 {
		b.SendMessage(&Message{
			Text: "Access denied.",
			Entities: []MessageEntity{{
				Offset: 0,
				Length: len("Access denied."),
				Type:   "italic",
			},
			},
		})
		return false
	}
	return true
}
