package config

import (
	"log"
	"os"
	"time"

	"gopkg.in/yaml.v2"
)

type Config struct {
	T string `yaml:"token"`
	D string `yaml:"delay"`
}

func init() {
	yfile, err := os.ReadFile("config.yaml")
	if err != nil {
		log.Fatal("Config.yaml:\t", err)
	}

	config = Config{}
	err = yaml.Unmarshal([]byte(yfile), &config)
	if err != nil {
		log.Fatal("Config.yaml:\t", err)
	}
}

var config Config

func GetConfig() Config {
	return config
}

func (c *Config) Token() string {
	return c.T
}

func (c *Config) Delay() (time.Duration, error) {
	return time.ParseDuration(c.D)
}
