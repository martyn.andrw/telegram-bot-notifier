package main

import (
	"botNotifier/internal/pkg/config"
	"botNotifier/internal/pkg/data_handler"
	"botNotifier/internal/pkg/logger"
	"botNotifier/internal/pkg/tg_api"
)

func main() {
	cfg := config.GetConfig()

	lggr := logger.NewLogger()
	go lggr.Start()

	stor := data_handler.NewRandStorage(lggr)

	bot := tg_api.NewBot(stor, &cfg, lggr)
	// go bot.Start()
	bot.Start()
}
